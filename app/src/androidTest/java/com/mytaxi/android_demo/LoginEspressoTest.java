package com.mytaxi.android_demo;

import android.Manifest;

import com.mytaxi.android_demo.activities.MainActivity;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

@RunWith(AndroidJUnit4.class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginEspressoTest extends BaseUtil{
    private static final String USERNAME = "crazydog335";
    private static final String PASSWORD = "venture";
    private static final String LOGOUT = "Logout";
    private static final String INCORRECT_PASSWORD = "video";

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mRuntimePermissionRule
            = GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION);

    /** Perform login with correct credentials and logout from app */
    @Test
    public void login(){
        performTypeOnId(R.id.edt_username, USERNAME);
        performTypeOnId(R.id.edt_password, PASSWORD);
        clickById(R.id.btn_login);
        waitInSecond(5);
        verifyIdIsDisplayed(R.id.textSearch);

        //perform logout
        openDrawer();
        verifyTextIsDisplayed(LOGOUT);
        clickByText(LOGOUT);
        verifyIdIsDisplayed(R.id.edt_username);
    }

    /** Perform login with blank username and blank password */
    @Test
    public void loginFailed_blankInfo(){
        verifyIdIsDisplayed(R.id.edt_username);
        verifyIdIsDisplayed(R.id.edt_password);
        clickById(R.id.btn_login);
        waitInSecond(1);
        verifyIdNotExist(R.id.textSearch);
    }

    /** Perform login with blank username */
    @Test
    public void loginFailed_blankUsername(){
        verifyIdIsDisplayed(R.id.edt_username);
        performTypeOnId(R.id.edt_password, PASSWORD);
        clickById(R.id.btn_login);
        waitInSecond(1);
        verifyIdNotExist(R.id.textSearch);
        verifyTextIsDisplayed(R.string.message_login_fail);
    }

    /** Perform login with blank password */
    @Test
    public void loginFailed_blankPassword(){
        performTypeOnId(R.id.edt_username, USERNAME);
        verifyIdIsDisplayed(R.id.edt_password);
        clickById(R.id.btn_login);
        waitInSecond(1);
        verifyIdNotExist(R.id.textSearch);
        verifyTextIsDisplayed(R.string.message_login_fail);
    }

    /** Perform login with incorrect password */
    @Test
    public void loginFailed_incorrectPassword(){
        performTypeOnId(R.id.edt_username, USERNAME);
        performTypeOnId(R.id.edt_password, INCORRECT_PASSWORD);
        clickById(R.id.btn_login);
        waitInSecond(1);
        verifyIdNotExist(R.id.textSearch);
        verifyTextIsDisplayed(R.string.message_login_fail);
    }
}
