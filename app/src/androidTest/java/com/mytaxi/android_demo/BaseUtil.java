package com.mytaxi.android_demo;

import android.view.View;

import org.hamcrest.Matcher;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.DrawerActions;

import java.util.concurrent.TimeUnit;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.equalToIgnoringCase;

public class BaseUtil {
    public static ViewAction waitForSecond(final long milis){
        return new ViewAction() {

            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for " + milis + " millis.";
            }

            @Override
            public void perform(UiController uiController, View view) {
                uiController.loopMainThreadForAtLeast(milis);
            }
        };
    }

    public static void waitInSecond(int second){
        onView(isRoot()).perform(waitForSecond(TimeUnit.SECONDS.toMillis(second)));
    }

    public static void clickById(int id){
        onView(withId(id)).perform(click());
    }

    public static void clickByText(String text){
        onView(withText(text)).perform(click());
    }

    public static void performTypeOnId(int id, String value){
        onView(withId(id)).perform(typeText(value), closeSoftKeyboard());
    }

    public static void verifyIdIsDisplayed(int id){
        onView(withId(id)).check(matches(isDisplayed()));
    }

    public static void verifyTextIsDisplayed(String text){
        onView(withText(text)).check(matches(isDisplayed()));
    }

    public static void verifyTextIsDisplayed(int id){
        onView(withText(id)).check(matches(isDisplayed()));
    }

    public static void verifyIdNotExist(int id){
        onView(withId(id)).check(doesNotExist());
    }

    public static void openDrawer(){
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
    }

    public static void assertWithText(int id, String text){
        onView(withId(id)).check(matches(withText(equalToIgnoringCase(text))));
    }
}
