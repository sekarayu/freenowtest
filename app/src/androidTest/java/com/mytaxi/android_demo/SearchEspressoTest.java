package com.mytaxi.android_demo;

import android.Manifest;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;

import com.mytaxi.android_demo.activities.MainActivity;


import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;

@RunWith(AndroidJUnit4.class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SearchEspressoTest extends BaseUtil{
    private static final String USERNAME = "crazydog335";
    private static final String PASSWORD = "venture";
    private static final String SEARCH_DRIVER = "Sara Christensen";

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mRuntimePermissionRule
            = GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION);

    @Before
    public void setUp(){
        performTypeOnId(R.id.edt_username, USERNAME);
        performTypeOnId(R.id.edt_password, PASSWORD);
        clickById(R.id.btn_login);
        waitInSecond(5);
        verifyIdIsDisplayed(R.id.textSearch);
    }

    /** Perform search by driver name and call */
    @Test
    public void searchAndCall(){
        performTypeOnId(R.id.textSearch, SEARCH_DRIVER);
        onView(withText(SEARCH_DRIVER)).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        onView(withText(SEARCH_DRIVER)).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                perform(click());
        assertWithText(R.id.textViewDriverName, SEARCH_DRIVER);
        verifyIdIsDisplayed(R.id.imageViewDriverAvatar);
        verifyIdIsDisplayed(R.id.textViewDriverLocation);
        verifyIdIsDisplayed(R.id.textViewDriverDate);
        verifyIdIsDisplayed(R.id.fab);
        clickById(R.id.fab);
    }
}
